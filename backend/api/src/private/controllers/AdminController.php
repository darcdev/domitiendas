<?php

namespace Controllers;

use Config\Database;
use Models\{Administrador , Tienda , Producto};
use Auth\Auth;
use PDO;


class AdminController extends BaseUserController {

    public $model_name = "Models\Administrador";

    public function createAdmin($request){
        $data = json_decode(file_get_contents("php://input"));

            
        if($request->getMethod() == 'POST'){

            if(!empty($data->nombre) &&
               !empty($data->password) &&
               !empty($data->correo)){

                $database = new Database();
                $db = $database->getConnection();
                $admin = new Administrador();
    
                $admin->setNombre($data->nombre);
                $admin->setApellido($data->apellido);
                $admin->setCorreo($data->correo);
                $admin->setPassword($data->password);

                
                $query = "INSERT INTO " . $admin->table_name . "(nombre , apellido , correo , password) VALUES(? , ? , ? , ?)";
                $stmt = $db->prepare($query);
                $stmt->bindParam(1, $data->nombre , PDO::PARAM_STR);
                $stmt->bindParam(2, $data->apellido , PDO::PARAM_STR);
                $stmt->bindParam(3, $data->correo , PDO::PARAM_STR);
            
        
                $password_hash = password_hash($data->password, PASSWORD_BCRYPT);
                $stmt->bindParam(4, $password_hash , PDO::PARAM_STR);
                $stmt->execute();        
                http_response_code(200);
             
                echo json_encode(array("message" => "admin was created."));
            }
             
            else{
             
                http_response_code(400);
             
                echo json_encode(array("message" => "Unable to create admin."));
            }
            

        }
    }

    public function updateAdmin($request){

        if($request->getMethod() == 'PATCH'){

            $token = $this->getToken($request);

            $auth = new Auth();
            $verified_token = $auth->validateJWT($token);
            
            if($verified_token){

                $rol = $this->getRol($token);

                if($rol == 'administradores'){
                    
                    $id = $request->getAttribute('id');
                    $data = json_decode(file_get_contents("php://input"));
        
                    $database = new Database();
                    $db = $database->getConnection();
            
                    $query = "UPDATE administradores
                                SET nombre = ? , apellido = ? , correo = ? , password = ? 
                                WHERE id = ? ";
            
                    $stmt = $db->prepare($query);
                        
                    $stmt->bindParam(1 , $data->nombre , PDO::PARAM_STR);
                    $stmt->bindParam(2 , $data->apellido , PDO::PARAM_STR);
                    $stmt->bindParam(3 , $data->correo , PDO::PARAM_STR);
                    $stmt->bindParam(4 , $data->password , PDO::PARAM_STR);
                    $stmt->bindParam(5 , $id , PDO::PARAM_INT);
            
                    if($stmt->execute()){
                        http_response_code(200);
                        echo json_encode(
                            array(
                                "message" => "successful query"
                            )
                        );
                    }else{
                        http_response_code(400);
                        echo "Row does not exist";
                    }

                }else{
                    http_response_code(401);
                    echo "Denied";    
                }
                
                           

            }else{
                http_response_code(401);
                echo "Denied";
            }

        }

    }


    public function agregarCategoriaTienda($request){

        if($request->getMethod() == 'POST'){

            $token = $this->getToken($request);

            $auth = new Auth();
            $verified_token = $auth->validateJWT($token);
            
            if($verified_token){

                $rol = $this->getRol($token);

                if($rol == 'administradores'){

                    $data = json_decode(file_get_contents("php://input"));
                    $category = strtolower($data->nombre_cat);
                    $tienda = new Tienda();
                    $category_exists = $tienda->categoryExists($category);
                    echo $category;
                    if(!$category_exists){
                        $tienda->addCategory($category);
                        http_response_code(200);
                        echo json_encode(
                            array(
                                "message" => "successful query"
                            )
                        );
                    }else{
                        http_response_code(401);
                        echo "Category already exists";
                    }

                }else{
                    http_response_code(401);
                    echo "Denied";
                }

            
            }else{
                http_response_code(401);
                echo "Denied";
            }
            

        }

    }

    
    public function agregarCategoriaProducto($request){

        if($request->getMethod() == 'POST'){

            $token = $this->getToken($request);

            $auth = new Auth();
            $verified_token = $auth->validateJWT($token);
            
            if($verified_token){

                $rol = $this->getRol($token);

                if($rol == 'administradores'){

                    $data = json_decode(file_get_contents("php://input"));
                    $category = ucfirst(strtolower($data->nombre_cat));
                    $producto = new Producto();
                    $category_exists = $producto->categoryExists($category);
        
                    if(!$category_exists){
                        $producto->addCategory($category);
                        http_response_code(200);
                        echo json_encode(
                            array(
                                "message" => "successful query"
                            )
                        );

                    }else{
                        http_response_code(401);
                        echo "Category already exists";
                    }

                }else{
                    http_response_code(401);
                    echo "Denied";
                }


            }else{
                http_response_code(401);
                echo "Denied";
            }
            

        }

    }


    public function actualizarCategoriaTienda($request){

        if($request->getMethod() == 'PATCH'){

            $token = $this->getToken($request);

            $auth = new Auth();
            $verified_token = $auth->validateJWT($token);
            
            if($verified_token){

                $rol = $this->getRol($token);

                if($rol == 'administradores'){

                    $id = $request->getAttribute('id');
                    $data = json_decode(file_get_contents("php://input"));
                    $category = $data->nombre_cat;
        
                    $database = new Database();
                    $db = $database->getConnection();
            
                    $query = "UPDATE categorias_tienda
                                SET nombre_cat = ? 
                                WHERE cod_cat = ? ";
            
                    $stmt = $db->prepare($query);
                        
                    $stmt->bindParam(1 , $category , PDO::PARAM_STR);
                    $stmt->bindParam(2 , $id , PDO::PARAM_STR);
            
                    if($stmt->execute()){
                        http_response_code(200);
                        echo json_encode(
                            array(
                                "message" => "successful query"
                            )
                        );

                    }else{
                        http_response_code(400);
                        echo "Row does not exist";
                    }

                }else{
                    http_response_code(401);
                    echo "Denied";
                }
                
            }else{
                http_response_code(401);
                echo "Denied";
            }

        }

    }


    public function actualizarCategoriaProducto($request){

        if($request->getMethod() == 'PATCH'){

            $token = $this->getToken($request);

            $auth = new Auth();
            $verified_token = $auth->validateJWT($token);
            
            if($verified_token){
                
                $rol = $this->getRol($token);
                
                if($rol == 'administradores'){

                    $id = $request->getAttribute('id');
                    $data = json_decode(file_get_contents("php://input"));
                    $category = $data->nombre_cat;
        
                    $database = new Database();
                    $db = $database->getConnection();
            
                    $query = "UPDATE categorias_productos
                              SET nombre_cat = ? 
                              WHERE cod_categoria = ? ";
            
                    $stmt = $db->prepare($query);
                        
                    $stmt->bindParam(1 , $category , PDO::PARAM_STR);
                    $stmt->bindParam(2 , $id , PDO::PARAM_STR);
            
                    if($stmt->execute()){
                        http_response_code(200);
                        echo json_encode(
                            array(
                                "message" => "successful query"
                            )
                        );

                    }else{
                        http_response_code(400);
                        echo "Row does not exist";
                    }
            

                }else{
                    http_response_code(401);
                    echo "Denied";
                }
            
                

            }else{
                http_response_code(401);
                echo "Denied";
            }

        }

    }


    public function obtenerRepartidor($request){

        if($request->getMethod() == 'GET'){

            $token = $this->getToken($request);
            
            $auth = new Auth();
            $verified_token = $auth->validateJWT($token);
            
            if($verified_token){

                $rol = $this->getRol($token);

                if($rol == 'administradores'){
                    
                    $id = $request->getAttribute('id');
    
                    $database = new Database();
                    $db = $database->getConnection();
                    
                    $query = "SELECT id , nombre , apellido
                              FROM repartidores
                              WHERE id = ?";
    
                    $stmt = $db->prepare($query);
                    $stmt->bindParam(1 , $id , PDO::PARAM_STR);
                    $stmt->execute();
                    $num = $stmt->rowCount();
    
                    if($num > 0){
                        $repartidor = $stmt->fetch(PDO::FETCH_OBJ);
                    
                        echo json_encode(
                            array(
                                "message" => "successful query",
                                "repartidor" => $repartidor
                            )
                        );
                    }

                }else{
                    http_response_code(401);             
                    echo json_encode(array("message" => "Access denied."));
                }
                
            }else{ 
                http_response_code(401);             
                echo json_encode(array("message" => "Access denied."));
            }
            
        }

    }


}
?>