import React, { useState, useEffect } from "react";

const ListDeliveries = function ListDeliveries() {
  const [deliveries, setDeliveries] = useState([]);

  useEffect(() => {
    async function getDeliveries() {
      const deliveries = [
        {
          logo:
            "https://images.rappi.pe/store_type/market-1598710455.png?d=200x200&e=webp",
          name: "Julio Cesar",
          deliveryId: "594949",
        },
      ];
      setDeliveries(deliveries);
    }
    getDeliveries();
  }, []);

  return (
    <>
      <div className="list-products-store row m-4">
        {deliveries.map((value, index) => {
          return (
            <div className="col-3 mb-4" key={index}>
              <div className="card border-0 rounded">
                <div>
                  <img src={value.logo} className="card-img-top" alt="..." />
                  <div className="card-body pt-2 p-0">
                    <h6 className="text-dark mt-0 text-center">{value.name}</h6>
                    <p className="text-small text-center">
                      Id Repartidor : #{value.deliveryId}
                    </p>
                    <br />
                  </div>
                </div>

                <hr />
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};
export default ListDeliveries;
