/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/interactive-supports-focus */
import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { logoutRequest } from "../actions";

import "../assets/styles/components/ListGroup.css";

const ListGroupDelivery = function ListGroupAdmin({ location }) {
  const action = location;
  const logoutHandler = () => {
    logoutRequest("home");
    window.location.href = "/loguear?usuario=repartidor";
  };
  return (
    <div className="list-group " id="list-tab" role="tablist">
      <Link
        to="/repartidor/2?action=pedidos-pendientes"
        className={
          "list-group-item  " +
          (action == "pedidos-pendientes" ? "active-section " : "")
        }
        id="pedidos-pendientes"
        data-toggle="list"
        role="tab"
        aria-controls="home"
      >
        Pedidos Pendientes
      </Link>
      <Link
        to="/repartidor/2?action=informacion-repartidor"
        className={
          "list-group-item  " +
          (action == "informacion-repartidor" ? "active-section " : "")
        }
        id="informacion-repartidor"
        data-toggle="list"
        role="tab"
        aria-controls="settings"
      >
        Información Repartidor
      </Link>
      <div
        onClick={logoutHandler}
        style={{ cursor: "pointer" }}
        className={"list-group-item bg-danger text-white "}
        id="informacion-usuario"
        data-toggle="list"
        role="tab"
        aria-controls="settings"
      >
        Cerrar Sesión
      </div>
    </div>
  );
};

const mapDispatchToProps = {
  logoutRequest,
};
export default connect(null, mapDispatchToProps)(ListGroupDelivery);
