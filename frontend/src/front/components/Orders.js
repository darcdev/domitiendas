import React, { useEffect, useState } from "react";
import ProductsOrderList from "./ProductsOrderList";

const Orders = function Orders({ user }) {
  const [orders, setOrders] = useState([]);
  const [update, setUpdate] = useState(false);

  useEffect(() => {
    async function getOrders() {
      console.log("jj");

      const order = [
        {
          orderId: "5678929",
          time: "4:00",
          orderDate: "2020-19-13",
          deliveryId: "#55666",
          clientId: "#5569",
          clientName: "rosabl camir",
          address: "555 av 6",
          state: false,
        },
      ];
      setOrders(order);
    }
    getOrders();
  }, [update]);

  const [activeOrder, setActiveOrder] = useState(false);

  const toggleOrderPanel = () => {
    setActiveOrder(!activeOrder);
  };

  const updateStateOrder = (event) => {
    setUpdate(!update);
  };
  return (
    <>
      <ul className="list-unstyled">
        {orders.map((value, index) => {
          return (
            <li key={index} className="media mb-3">
              <div className="media-body">
                <h5 className="mt-0 mb-2">
                  Pedido #{value.orderId}{" "}
                  <span className="badge badge-warning">
                    Pedido {value.state ? "activo" : "inactivo"}
                  </span>
                </h5>
                <p>
                  <span className="text-dark">Fecha de Pedido :</span>{" "}
                  {value.orderDate} -
                  <span className="text-dark"> Hora de Pedido :</span>{" "}
                  {value.time}
                </p>

                {user != "store" && user != "delivery" ? (
                  <p className="">
                    <span className="text-dark ">Id Repartidor :</span>{" "}
                    {value.deliveryId}
                  </p>
                ) : (
                  ""
                )}
                {user != "store" ? (
                  <p className="">
                    <span className="text-dark ">Id Cliente :</span>{" "}
                    {value.clientId}
                  </p>
                ) : (
                  ""
                )}
                {user != "store" && user != "admin" ? (
                  <p className="">
                    <span className="text-dark ">Nombre Cliente :</span>{" "}
                    {value.clientName}
                  </p>
                ) : (
                  ""
                )}
                {user != "store" ? (
                  <p className="">
                    <span className="text-dark ">Dirección :</span>{" "}
                    {value.address}
                  </p>
                ) : (
                  ""
                )}
                <button
                  onClick={toggleOrderPanel}
                  className="btn btn-primary btn-color-primary btn-size-normal mt-2 mr-2"
                >
                  Ver Productos
                </button>

                {user == "delivery" ? (
                  <button
                    onClick={updateStateOrder}
                    className="btn btn-success btn-size-normal mt-2"
                  >
                    Entregado
                  </button>
                ) : (
                  ""
                )}
                <hr />
              </div>
            </li>
          );
        })}
      </ul>
      <div>
        {activeOrder ? (
          <ProductsOrderList
            show={activeOrder}
            handleClose={toggleOrderPanel}
          />
        ) : (
          ""
        )}
      </div>
    </>
  );
};

export default Orders;
