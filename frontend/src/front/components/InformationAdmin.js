import React, { useState, useEffect } from "react";

const InformationAdmin = function InformationAdmin() {
  const [adminData, setAdminData] = useState({
    name: "",
    lastname: "",
    email: "",
    password: "",
    validation: false,
  });

  useEffect(() => {
    async function getAdminData() {
      const data = {
        name: "andres",
        lastname: "garcia",
        email: "merca@gmail.com",
        password: "password",
      };
      setAdminData(data);
    }
    getAdminData();
  }, []);

  const handleValidation = () => {
    const { name, lastname, email, password } = adminData;
    if (!(name && lastname && email && password)) {
      setAdminData({
        ...adminData,
        validation: true,
      });
    } else {
      setAdminData({
        ...adminData,
        validation: false,
      });
    }
  };
  const handleInput = (event) => {
    setAdminData({
      ...adminData,
      [event.target.name]: event.target.value,
    });
  };
  const handleUpdateAdminData = (event) => {
    event.preventDefault();
    handleValidation();
  };

  return (
    <>
      <div className="row">
        <div className="col-7">
          <h3 className="mb-4">Datos del Administrador</h3>
          <form>
            <div className="form-row">
              <div className="form-group col-md-12">
                <label htmlFor="name">Nombre</label>
                <input
                  type="text"
                  className="form-control"
                  id="name"
                  placeholder="Nombre"
                  name="name"
                  value={adminData.name}
                  onChange={handleInput}
                />
              </div>
              <div className="form-group col-md-12">
                <label htmlFor="lastname">Apellido</label>
                <input
                  type="text"
                  className="form-control"
                  id="lastname"
                  placeholder="Apellido"
                  name="lastname"
                  value={adminData.lastname}
                  onChange={handleInput}
                />
              </div>
              <div className="form-group col-md-12">
                <label htmlFor="email">Email</label>
                <input
                  type="email"
                  className="form-control"
                  id="email"
                  name="email"
                  value={adminData.email}
                  placeholder="Email"
                  onChange={handleInput}
                />
              </div>

              <div className="form-group col-md-12">
                <label htmlFor="password">Contraseña</label>
                <input
                  type="password"
                  className="form-control"
                  id="password"
                  name="password"
                  value={adminData.password}
                  placeholder="Contraseña"
                  onChange={handleInput}
                />
              </div>
            </div>
            {adminData.validation ? (
              <div className="mb-4 mb-3" style={{ color: "red" }}>
                Por favor rellene todos los campos
              </div>
            ) : (
              ""
            )}
            <button
              type="submit"
              onClick={handleUpdateAdminData}
              className="btn btn-success"
            >
              Actualizar
            </button>
          </form>
        </div>
      </div>
    </>
  );
};

export default InformationAdmin;
