import React, { useState } from "react";
import { connect } from "react-redux";
import { fade, makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import InputBase from "@material-ui/core/InputBase";
import Badge from "@material-ui/core/Badge";
import MenuIcon from "@material-ui/icons/Menu";
import SearchIcon from "@material-ui/icons/Search";
import AccountCircle from "@material-ui/icons/AccountCircle";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import logo from "../assets/static/img/logo.png";
import DrawerLeft from "./DrawerLeft";
import CartPanel from "./CartPanel";
import CloseIcon from "@material-ui/icons/Close";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";

const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(1),
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.12),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  inputRoot: {
    color: "inherit",
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex",
    },
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
}));

const Header = function Header({ user, rol, cartProducts, match }) {
  const [drawerOpenState, setDrawerState] = useState(false);
  const [cartPanelState, setCartPanelState] = useState(false);
  const classes = useStyles();
  const param = window.location.pathname.split("/")[1];
  /**** Drawer ****/

  const toggleDrawer = (open, option) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    if (option == 1) {
      setDrawerState(open);
    } else if (option == 2) {
      setCartPanelState(open);
    }
  };
  return (
    <div className={classes.grow}>
      <AppBar position="static">
        <Toolbar>
          {param != "repartidor" && param != "admin" && param != "tienda" ? (
            <IconButton
              onClick={toggleDrawer(!drawerOpenState, 1)}
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="open drawer"
            >
              {drawerOpenState ? <CloseIcon /> : <MenuIcon />}
            </IconButton>
          ) : (
            ""
          )}
          <Typography className={classes.title} variant="h6" noWrap>
            <Link to="/">
              <img width="120" src={logo} alt="dominosexpress" />
            </Link>
          </Typography>

          {param != "repartidor" && param != "admin" && param != "tienda" ? (
            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <InputBase
                placeholder="Buscar"
                classes={{
                  colors: classes.colord,
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
                inputProps={{ "aria-label": "search" }}
              />
            </div>
          ) : (
            ""
          )}

          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            {param != "repartidor" && param != "admin" && param != "tienda" ? (
              <IconButton
                aria-label={`show ${cartProducts.length}  new notifications`}
                color="inherit"
              >
                <Badge
                  {...(cartProducts.length > 0
                    ? { badgeContent: cartProducts.length }
                    : {})}
                  color={"secondary"}
                >
                  <ShoppingCartIcon
                    onClick={toggleDrawer(!drawerOpenState, 2)}
                  />
                </Badge>
              </IconButton>
            ) : (
              ""
            )}
          </div>
          {console.log(rol)}
          {param != "repartidor" && param != "admin" && param != "tienda"
            ? [
                user.email ? (
                  <Link
                    key={1}
                    to="/usuario/2?action=mis-pedidos"
                    style={{ color: "white" }}
                  >
                    <AccountCircle
                      style={{
                        fontSize: 40,
                        marginLeft: "2rem",
                        cursor: "pointer",
                        color: "green[500]",
                      }}
                    />
                  </Link>
                ) : (
                  <Link
                    key={2}
                    to="/loguear?usuario=cliente"
                    style={{ color: "white" }}
                  >
                    <Button
                      style={{ marginLeft: "2rem" }}
                      variant="contained"
                      color="primary"
                      className={classes.button}
                      startIcon={<AccountCircle />}
                    >
                      Ingresar
                    </Button>
                  </Link>
                ),
              ]
            : ""}
        </Toolbar>
      </AppBar>
      <DrawerLeft
        onClick={toggleDrawer}
        toggleDrawer={toggleDrawer}
        drawerOpenState={drawerOpenState}
      />
      <CartPanel
        onClick={toggleDrawer}
        toggleDrawer={toggleDrawer}
        drawerOpenState={cartPanelState}
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  var products = state.cart.products;
  return {
    user: state.user,
    rol: state.user.rol,
    cartProducts: products,
  };
};

export default connect(mapStateToProps, null)(withRouter(Header));
