import React from "react";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";

import Home from "../containers/Home";
import "../assets/styles/normalize.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "../assets/styles/App.css";
import Layout from "../components/Layout";
import Login from "../containers/Login";
import Register from "../containers/Register";
import StorePanel from "../containers/Store";
import DeliveryPanel from "../containers/Delivery";
import AdminPanel from "../containers/Admin";
import UserPanel from "../containers/User";
import CategoryStore from "../containers/CategoryStore";
import ScrollToTop from "../components/ScrollToTop";
import ProductsStore from "../containers/ProductsStore";
const App = function App({ user }) {
  return (
    <BrowserRouter>
      <ScrollToTop>
        <Layout>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/loguear" component={Login} />
            <Route exact path="/registrar" component={Register} />
            <Route
              exact
              path="/tienda/:id"
              render={(props) =>
                !(user == "tienda") ? (
                  <StorePanel {...props} />
                ) : (
                  <Redirect to="/loguear?usuario=tienda" />
                )
              }
            ></Route>
            <Route
              exact
              path="/repartidor/:id"
              render={(props) =>
                !(user == "repartidor") ? (
                  <DeliveryPanel {...props} />
                ) : (
                  <Redirect to="/loguear?usuario=repartidor" />
                )
              }
            ></Route>
            <Route
              exact
              path="/admin/:id"
              render={(props) =>
                !(user == "admin") ? (
                  <AdminPanel {...props} />
                ) : (
                  <Redirect to="/loguear?usuario=admin" />
                )
              }
            ></Route>
            <Route
              exact
              path="/usuario/:id"
              render={(props) =>
                !(user == "usuario") ? (
                  <UserPanel {...props} />
                ) : (
                  <Redirect to="/loguear?usuario=tienda" />
                )
              }
            ></Route>
            <Route
              exact
              path="/tiendas/tipo/:category"
              component={CategoryStore}
            />
            <Route exact path="/tiendas/:name" component={ProductsStore} />
          </Switch>
        </Layout>
      </ScrollToTop>
    </BrowserRouter>
  );
};

export default App;
