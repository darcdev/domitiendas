import React from "react";
import Category from "./Category";
const CategoryPanel = function CategoryPanel({ activePanel, handleClose }) {
  return (
    <>
      {activePanel ? (
        <div className="row">
          <div className="col-12">
            <Category show={activePanel} handleClose={handleClose} />
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  );
};

export default CategoryPanel;
