import React from "react";
import ReactDOM from "react-dom";
import { createStore, compose, applyMiddleware } from "redux";
import reduxThunk from "redux-thunk";
import { Provider } from "react-redux";
import reducers from "./reducers";
import App from "./routes/App";
import { getCookie } from "../front/utils/cookies";

if (!window.localStorage.getItem("cart")) {
  window.localStorage.setItem("cart", JSON.stringify([]));
}
if (!window.localStorage.getItem("totalProducts")) {
  window.localStorage.setItem("totalProducts", JSON.stringify(0));
}
const initialStore = {
  user: {
    rol: getCookie("role") ? getCookie("role") : "home",
    email: getCookie("email") ? getCookie("email") : "",
  },
  cart: {
    products: JSON.parse(window.localStorage.getItem("cart")),
  },
};
const store = createStore(
  reducers,
  initialStore,
  compose(applyMiddleware(reduxThunk))
);

const user = initialStore.user.rol;

ReactDOM.render(
  <Provider store={store}>
    <App isLogged={user} />
  </Provider>,
  document.getElementById("app")
);
