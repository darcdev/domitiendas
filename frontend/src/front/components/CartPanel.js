import React, { useState, useEffect } from "react";
import clsx from "clsx";
import { connect } from "react-redux";
import { emptyBasket } from "../actions";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import "../assets/styles/components/CartPanel.css";
import WithoutProducts from "./WithoutProducts";
import CartProducts from "./CartProducts";
import { getTotal } from "../utils/cart";
const useStyles = makeStyles({
  list: {
    width: 600,
    height: "100%",
    padding: "0.5rem 0.5rem",
  },
  fullList: {
    width: "auto",
  },
  paper: {
    height: "calc(100% - 70px)",
    top: 70,
    right: "0rem",
  },
  BackdropProps: {
    backgroundColor: "rgba(0, 0, 0, 0)",
  },
});

const TemporaryDrawer = function TemporaryDrawer({
  cartProducts,
  emptyBasket,
  toggleDrawer,
  drawerOpenState,
}) {
  const emptyBasketHandle = () => {
    emptyBasket([]);
  };
  const classes = useStyles();
  const list = () => (
    <div
      style={{ padding: "1rem" }}
      className={clsx(classes.list, {
        [classes.fullList]: false,
      })}
      role="presentation"
      onClick={toggleDrawer(false)}
      onKeyDown={toggleDrawer(false)}
    >
      <div className="row p-0 m-0" style={{ display: "block", width: "100%" }}>
        <div className="col-12">
          <h5 className="text-muted">Entregar En : </h5>
          <h2>Direccion</h2>
        </div>
      </div>
      <hr />
      {cartProducts.length == 0 ? (
        <WithoutProducts />
      ) : (
        <>
          <div className="cart-products">
            <CartProducts />
          </div>

          <div className="cart-details mt-2">
            <button onClick={emptyBasketHandle} className="btn cart-empty mr-1">
              Vaciar Canasta
            </button>
            <button className="btn cart-send">
              <span
                style={{
                  display: "inline-block",
                  position: "absolute",
                  left: "8px",
                  borderRadius: "50%",
                  width: "30px",
                  backgroundColor: "black",
                }}
              >
                {cartProducts.length}
              </span>
              Hacer Pedido{" "}
              <span
                style={{
                  display: "inline-block",
                  position: "absolute",
                  right: "10px",
                  borderRadius: "20%",
                  width: "70px",
                  backgroundColor: "black",
                }}
              >
                ${getTotal(cartProducts)}
              </span>
            </button>
          </div>
        </>
      )}
    </div>
  );

  return (
    <div>
      <Drawer
        ModalProps={{
          BackdropProps: {
            classes: {
              root: classes.BackdropProps,
            },
          },
        }}
        anchor="right"
        classes={{ paper: classes.paper }}
        open={drawerOpenState}
        onClose={toggleDrawer(false, 2)}
      >
        {list()}
      </Drawer>
    </div>
  );
};

const mapStateToProps = (state) => {
  var products = state.cart.products;
  return {
    cartProducts: products,
  };
};

const mapDispatchToProps = {
  emptyBasket,
};
export default connect(mapStateToProps, mapDispatchToProps)(TemporaryDrawer);
