/* eslint-disable jsx-a11y/no-onchange */
import React, { useState, useEffect } from "react";

const Categories = function Categories() {
  const [categoryStore, setCategoryStore] = useState({
    new: "",
    actualCategoryStore: "",
  });
  const [categoryProduct, setCategoryProduct] = useState({
    new: "",
    actualCategoryStore: "",
    actualCategoryProduct: "",
  });
  const [categoriesStore, setStoreCategories] = useState([]);
  const [categoriesProduct, setCategoriesProduct] = useState([]);

  useEffect(() => {
    console.log("hola");
    async function getCategoriesStore() {
      const categoriesStore = ["supermercados", "restaurantes", "mascotas"];
      const subcategories = ["Accesorios", "detalles", "comidas rapidas"];

      setStoreCategories(categoriesStore);
      setCategoriesProduct(subcategories);

      setCategoryStore({
        ...categoryStore,
        actualCategoryStore: categoriesStore[0],
      });
      setCategoryProduct({
        ...categoryProduct,
        actualCategoryStore: categoriesStore[0],
        actualCategoryProduct: subcategories[0],
      });
    }

    getCategoriesStore();
  }, []);

  const handleInputStore = (event) => {
    setCategoryStore({
      ...categoryStore,
      new: event.target.value,
    });
  };

  const handleInputProducts = (event) => {
    setCategoryProduct({
      ...categoryProduct,
      new: event.target.value,
    });
  };

  const handleActualStore = (event) => {
    setCategoryStore({
      ...categoryStore,
      actualCategoryStore: event.target.value,
    });
  };

  const handleActualStoreProduct = (event) => {
    setCategoryProduct({
      ...categoryProduct,
      actualCategoryStore: event.target.value,
    });
  };

  const handleActualProduct = (event) => {
    setCategoryProduct({
      ...categoryProduct,
      actualCategoryStore: event.target.value,
    });
  };

  const updateAddCategoryStore = (action) => {
    if (action == "add") {
      ///
    } else if (action == "update") {
      //
    }
  };

  const updateAddCategoryProduct = (action) => {
    if (action == "add") {
      ///
    } else if (action == "update") {
      //
    }
  };

  return (
    <>
      <h4 className="mb-4">Categorias Tiendas</h4>
      <div className="row mb-4">
        <div className="col-7">
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <label className="input-group-text" htmlFor="actualCategoryStore">
                Categoria
              </label>
            </div>
            <select
              className="custom-select"
              id="actualCategoryStore"
              name="actualCategoryStore"
              style={{ width: "auto" }}
              onChange={handleActualStore}
            >
              {categoriesStore.map((value, index) => {
                return (
                  <option key={index} value={value}>
                    {value}
                  </option>
                );
              })}
            </select>
          </div>
        </div>
        <div className="col-6">
          <div className="form-group">
            <input
              type="text"
              className="form-control mb-2"
              id="inputPassword4"
              placeholder="Categoria"
              onChange={handleInputStore}
            />
            <button
              className="btn btn-success mr-3"
              onClick={() => updateAddCategoryStore("add")}
            >
              Agregar
            </button>
            <button
              className="btn btn-warning"
              onClick={() => updateAddCategoryStore("update")}
            >
              Actualizar
            </button>
          </div>
        </div>
      </div>
      <hr />
      <h4 className="mb-4">Categorias Productos </h4>
      <div className="row">
        <div className="col-7">
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <label
                className="input-group-text"
                htmlFor="actualCategoryProduct"
              >
                Por Categoria de la tienda
              </label>
            </div>
            <select
              className="custom-select"
              id="actualCategoryProduct"
              name="actualCategoryProduct"
              style={{ width: "auto" }}
              onChange={handleActualStoreProduct}
            >
              {categoriesStore.map((value, index) => {
                return (
                  <option key={index} value={value}>
                    {value}
                  </option>
                );
              })}
            </select>
          </div>
        </div>
        <div className="col-7">
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <label className="input-group-text" htmlFor="inputGroupSelect01">
                Categoria
              </label>
            </div>
            <select
              className="custom-select"
              id="inputGroupSelect01"
              style={{ width: "auto" }}
              onChange={handleActualProduct}
            >
              {categoriesProduct.map((value, index) => {
                return (
                  <option key={index} value={value}>
                    {value}
                  </option>
                );
              })}
            </select>
          </div>
        </div>
        <div className="col-6">
          <div className="form-group">
            <input
              type="text"
              className="form-control mb-2"
              id="inputPassword4"
              placeholder="Categoria"
              onChange={handleInputProducts}
            />
            <button
              className="btn btn-success mr-3"
              onClick={() => updateAddCategoryProduct("add")}
            >
              Agregar
            </button>
            <button
              className="btn btn-warning"
              onClick={() => updateAddCategoryProduct("update")}
            >
              Actualizar
            </button>
          </div>
        </div>
      </div>
    </>
  );
};
export default Categories;
