import React, { useState } from "react";
import { connect } from "react-redux";
import { loginUser } from "../actions";
import { Link, Redirect } from "react-router-dom";
import "../assets/styles/components/Login.css";

const Login = function Login({ location, loginUser }) {
  const [form, setFormValues] = useState({
    email: "",
    password: "",
    validation: false,
  });

  const handleInput = (event) => {
    setFormValues({
      ...form,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    handleValidation();
    const user = location.search.split("=")[1];
    loginUser(form, user);
  };

  const handleValidation = () => {
    if (!(form.email && form.password)) {
      setFormValues({ ...form, validation: true });
      return false;
    } else {
      setFormValues({ ...form, validation: false });
      return true;
    }
  };
  return (() => {
    if (
      location.search === "?usuario=cliente" ||
      location.search === "?usuario=tienda" ||
      location.search === "?usuario=repartidor" ||
      location.search === "?usuario=admin"
    ) {
      return (
        <div className="limiter">
          <div className="container-login100">
            <div className="wrap-login100">
              <div className="login100-form-title">
                <span className="login100-form-title-1">Iniciar Sesión</span>
              </div>

              <form className="login100-form validate-form">
                <div
                  className="mb-4 wrap-input100 validate-input m-b-26"
                  data-validate="Username is required"
                >
                  <span className="label-input100">Correo</span>
                  <input
                    className="input100"
                    type="text"
                    name="email"
                    value={form.email}
                    placeholder="Digite el correo"
                    onChange={handleInput}
                  />
                  <span className="focus-input100"></span>
                </div>

                <div
                  className="mb-4 wrap-input100 validate-input m-b-18"
                  data-validate="Password is required"
                >
                  <span className="label-input100">Contraseña</span>
                  <input
                    className="input100"
                    type="password"
                    name="password"
                    placeholder="Digite la contraseña"
                    value={form.password}
                    onChange={handleInput}
                  />
                  <span className="focus-input100"></span>
                </div>

                <div
                  id="validation-user"
                  className="mb-3"
                  style={{ display: "none", color: "red" }}
                >
                  Los datos son Incorrectos , Intente de nuevo
                </div>
                {form.validation ? (
                  <div className="mb-4 mb-3" style={{ color: "red" }}>
                    Por favor rellene todos los campos
                  </div>
                ) : (
                  ""
                )}
                <div className="mb-4 container-login100-form-btn">
                  <button className="login100-form-btn" onClick={handleSubmit}>
                    Iniciar Sesión
                  </button>
                </div>

                <div>
                  {(() => {
                    if (location.search === "?usuario=tienda") {
                      return (
                        <small>
                          No tienes una tienda registrada?
                          <Link to="/registrar?usuario=tienda">
                            {" "}
                            Registrar Tienda
                          </Link>
                        </small>
                      );
                    } else if (location.search === "?usuario=repartidor") {
                      return (
                        <small>
                          No tienes Cuenta ?
                          <Link to="/registrar?usuario=repartidor">
                            Registrate
                          </Link>
                        </small>
                      );
                    } else if (location.search === "?usuario=cliente") {
                      return (
                        <small>
                          No tienes Cuenta ?
                          <Link to="/registrar?usuario=cliente">
                            Registrate
                          </Link>
                        </small>
                      );
                    }
                  })()}
                </div>
              </form>
            </div>
          </div>
        </div>
      );
    } else {
      return <Redirect to="/" />;
    }
  })();
};

const mapDispatchToProps = {
  loginUser,
};

export default connect(null, mapDispatchToProps)(Login);
