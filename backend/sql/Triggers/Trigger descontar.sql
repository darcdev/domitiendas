CREATE OR REPLACE FUNCTION descontar() RETURNS TRIGGER AS $$
	DECLARE	
			
	_cantidad integer;
	BEGIN
		SELECT stock INTO _cantidad  from productos  WHERE cod_producto  = NEW.cod_producto;

		UPDATE productos SET stock  = _cantidad - NEW.cantidad WHERE cod_producto = NEW.cod_producto;
		RETURN NULL;
	END
$$LANGUAGE plpgsql;
	

CREATE TRIGGER descontar_stock AFTER INSERT ON detalle FOR EACH ROW EXECUTE PROCEDURE descontar();
