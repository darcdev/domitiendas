import React, { useState, useEffect } from "react";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
const customStyles = {
  overlay: { zIndex: 2000 },
};
const ProductsOrderList = function ProductsOrderList({ show, handleClose }) {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    async function getProductDetails() {
      const productDetails = [
        {
          logo:
            "https://cdn4.buysellads.net/uu/1/76611/1602785884-blackfire_04_2x.png",
          idProduct: "453930",
          name: "Pan Blanco Bimbo x12",
          storename: "Exitos Express",
          price: 200.1,
          cant: 10,
          notes:
            "Con tortillas de 12 mg y un poco de mantequillaCon tortillas de 12 mg y un poco de mantequilla Con tortillas de 12 mg y un poco de mantequilla Con tortillas de 12 mg y un poco de mantequilla",
        },
      ];

      setProducts(productDetails);
    }

    getProductDetails();
  }, []);

  return (
    <Modal style={customStyles} size="lg" show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Detalle Pedido</Modal.Title>
        <hr />
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            {products.map((value, index) => {
              return (
                <Col key={index} md={12}>
                  <div className="media mb-3">
                    <img
                      style={{ width: "150px" }}
                      src={value.logo}
                      className="align-self-center mr-3"
                      alt="..."
                    />
                    <div className="media-body">
                      <h5 className="mt-0 mb-3">{value.name}</h5>
                      <p>
                        <span className="text-dark">Numero de Producto :</span>{" "}
                        {value.idProduct}{" "}
                      </p>
                      <p>
                        <span className="text-dark">Tienda :</span>{" "}
                        {value.storename}{" "}
                      </p>
                      <p>
                        {" "}
                        <span className="text-dark">Precio :</span> $
                        {value.price}{" "}
                      </p>

                      <p>
                        {" "}
                        <span className="text-dark">Cantidad :</span>{" "}
                        {value.cant}{" "}
                      </p>

                      <p>
                        <span className="text-dark">Nota Adicional : </span>{" "}
                        {value.notes}
                      </p>
                      <hr />
                    </div>
                  </div>
                </Col>
              );
            })}
          </Row>
        </Container>
      </Modal.Body>
    </Modal>
  );
};

export default ProductsOrderList;
