import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import ProductPanel from "../components/ProductPanel";
import { addProductCart, incrementProduct, decrementProduct } from "../actions";
import { getItem } from "../utils/cart";
const ProductsStore = function ProductsStore({
  incrementProduct,
  decrementProduct,
  match,
  addProductCart,
  cartProducts,
}) {
  const [activeProduct, setActiveProduct] = useState({
    active: false,
    data: "",
  });
  const [storeCategory, setStoreCategory] = useState("");
  const [products, setProducts] = useState([]);
  const [storeCategories, setStoreCategories] = useState([]);

  useEffect(() => {
    async function getCategoryStore() {
      setStoreCategory("supermecados");
    }

    async function getCategories() {
      const storeCatego = ["Verduras", "Carne", "Frutas", "Bebidas"];
      setStoreCategories(storeCatego);
    }

    async function getProductsByCategory() {
      const products = [
        {
          id: 1,
          name: "Pan Bimbo 12 unidades",
          price: 5850,
          stock: 10,
          description: "los mejores aceites del llano",
          poster:
            "https://images.rappi.cl/store_type/hogar-1601559447.png?d=200x200&e=webp",
        },
        {
          id: 2,
          name: "Aceite grasosito xD",
          price: 5850,
          stock: 50,
          description: "los mejores aceites del llano",
          poster:
            "https://images.rappi.cl/store_type/hogar-1601559447.png?d=200x200&e=webp",
        },
        {
          id: 3,
          name: "Panela la riata",
          price: 3200,
          stock: 20,
          description: "los mejores aceites del llano",
          poster:
            "https://images.rappi.cl/store_type/hogar-1601559447.png?d=200x200&e=webp",
        },
      ];
      setProducts(products);
    }
    getCategoryStore();
    getCategories();
    getProductsByCategory();
  }, []);

  const toggleProductPanel = (value) => {
    setActiveProduct({
      ...activeProduct,
      active: !activeProduct.active,
      data: value ? value : "",
    });
  };

  const addProductToCart = (value) => {
    if (!getItem(value.id, cartProducts)) {
      addProductCart(value);
    }
  };

  const handleIncrement = (id) => {
    incrementProduct(id);
  };
  const handleDecrement = (id) => {
    decrementProduct(id);
  };
  const nameStore = match.params.name;

  return (
    <div className="container-fluid">
      <div className="row mt-4">
        <div className="col-12">
          <h3 className="mb-3">
            {storeCategory.charAt(0).toUpperCase() + storeCategory.slice(1) ||
              " "}{" "}
            &gt; {nameStore.charAt(0).toUpperCase() + nameStore.slice(1)}
          </h3>
        </div>
      </div>
      <div className="row mb-5">
        <div className="col-12">
          <hr />
          <h3 className="ml-3 text-center">Productos por Categoria</h3>
        </div>
      </div>
      <div className="row mt-5">
        <div className="col-12">
          <ProductPanel
            addProduct={activeProduct.data}
            activeProduct={activeProduct.active}
            handleClose={toggleProductPanel}
          />
        </div>
      </div>
      <div className="row m-auto  " style={{ width: "95%" }}>
        {storeCategories.map((value, index) => {
          return (
            <>
              <div className="mb-3 col-12" key={index}>
                <h4>Verduras</h4>
                <hr />
              </div>

              {products.map((value, index) => {
                return (
                  <div className="col-2 mb-5" key={index}>
                    <div className="card border-0 rounded">
                      <Link to="#" onClick={() => toggleProductPanel(value)}>
                        <img
                          src={value.poster}
                          className="card-img-top"
                          alt="..."
                        />{" "}
                        <div className="card-body pt-2 p-0">
                          <b className="text-dark">${value.price}</b>
                          <br />
                          <p className="text-dark mt-0">{value.name}</p>
                          <br />
                        </div>
                      </Link>

                      {getItem(value.id, cartProducts) ? (
                        <div className="cont-info-simulate">
                          <button
                            onClick={() => handleDecrement(value.id)}
                            className="mr-4"
                          >
                            {getItem(value.id, cartProducts).cant == 1 ? (
                              <img
                                style={{ width: "15px" }}
                                src="https://img.icons8.com/android/24/000000/trash.png"
                                alt=""
                              />
                            ) : (
                              "-"
                            )}
                          </button>
                          {getItem(value.id, cartProducts).cant}{" "}
                          <button
                            onClick={() => handleIncrement(value.id)}
                            className="ml-4"
                          >
                            {" "}
                            {getItem(value.id, cartProducts).cant < value.stock
                              ? "+"
                              : ""}
                          </button>
                        </div>
                      ) : (
                        <button
                          onClick={() => addProductToCart(value)}
                          style={{
                            fontSize: "0.9rem",
                            backgroundColor: "#3f51b5",
                          }}
                          type="button"
                          className="btn btn-block mt-0 btn-primary"
                          data-toggle="button"
                          aria-pressed="false"
                        >
                          Agregar
                        </button>
                      )}
                    </div>
                  </div>
                );
              })}
            </>
          );
        })}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  var products = state.cart.products;
  return {
    cartProducts: products,
  };
};
const mapDispatchToProps = {
  addProductCart,
  incrementProduct,
  decrementProduct,
};
export default connect(mapStateToProps, mapDispatchToProps)(ProductsStore);
