import React from "react";
import InformationDelivery from "../components/InformationDelivery";
import ListGroupDelivery from "../components/ListGroupDelivery";
import Orders from "../components/Orders";

const DeliveryPanel = function DeliveryPanel({ location }) {
  const action = location.search.split("=")[1];
  return (
    <div className="container-fluid mt-4">
      <div className="row">
        <div className="col-12">
          <h2 className="mb-3">
            DomiRepartidores
            <span
              style={{ fontSize: "0.98rem" }}
              className="badge badge-secondary"
            >
              Panel Administrativo
            </span>
          </h2>
          <hr />
        </div>
      </div>
      <div className="row mt-5 mb-5">
        <div className="col-4">
          <ListGroupDelivery location={action} />
        </div>

        <div className="col-8 position">
          <div className="tab-content" id="nav-tabContent col-12">
            <div
              className="tab-pane fade show active"
              id="list-home"
              role="tabpanel"
              aria-labelledby="list-home-list"
            ></div>
            {action == "pedidos-pendientes" ? <Orders user="delivery" /> : ""}
            {action == "informacion-repartidor" ? <InformationDelivery /> : ""}
          </div>
        </div>
      </div>
    </div>
  );
};

export default DeliveryPanel;
