/* eslint-disable jsx-a11y/no-onchange */
import React, { useState, useEffect } from "react";
import ProductDetailStore from "./ProductDetailStore";

const ProductsByStore = function ProductsByStore() {
  const [activeProduct, setActiveProduct] = useState({
    idProduct: "",
    state: false,
  });
  const [activeCategory, setActiveCategory] = useState("");
  const [categories, setCategories] = useState([]);
  const [products, setProducts] = useState([]);
  const toggleProductStore = (id) => {
    const data = id
      ? { ...activeProduct, idProduct: id, state: !activeProduct.state }
      : { ...activeProduct, state: !activeProduct.state };

    setActiveProduct(data);
  };

  useEffect(() => {
    const categories = ["supermercados", "restaurantes", "relojeria"];
    setCategories(categories);
    setActiveCategory(categories[0]);
  }, []);

  useEffect(() => {
    const products = [
      {
        idProduct: "5648494",
        name: "pan bimbo 12 unidades",
        price: 1500,
        cant: 100,
      },
    ];
    setProducts(products);
  }, [activeCategory]);

  const handleCategory = (event) => {
    setActiveCategory(event.target.value);
  };

  const handleDeleteProduct = (id) => {
    //eliminar product -> estado inactivo
  };

  return (
    <>
      <div className="row">
        <div className="col-6">
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <label className="input-group-text" htmlFor="inputGroupSelect01">
                Categoria
              </label>
            </div>
            <select
              className="custom-select"
              id="inputGroupSelect01"
              style={{ width: "auto" }}
              value={activeCategory}
              onChange={handleCategory}
            >
              {categories.map((value, index) => {
                return (
                  <option key={index} value={value}>
                    {value}
                  </option>
                );
              })}
            </select>
          </div>
        </div>
      </div>
      <div className="list-products-store row m-4">
        {products.map((value, index) => {
          return (
            <div className="col-3 mb-4" key={index}>
              <div className="card border-0 rounded">
                <div>
                  <img
                    src="https://images.rappi.pe/store_type/market-1598710455.png?d=200x200&e=webp"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body pt-2 p-0">
                    <p className="text-small">
                      Id Producto : #{value.idProduct}
                    </p>
                    <p className="text-dark mt-0">{value.name}</p>
                    <p className="text-dark">Precio : {value.name}</p>
                    <p className="text-dark">Cantidad : {value.cant}</p>
                    <br />
                  </div>
                </div>
                <button
                  onClick={() => toggleProductStore(value.idProduct)}
                  style={{ fontSize: "0.9rem" }}
                  type="button"
                  className="btn btn-block mt-0 btn-success mb-1"
                  data-toggle="button"
                  aria-pressed="false"
                >
                  Editar
                </button>
                <button
                  onClick={() => handleDeleteProduct(1)}
                  style={{ fontSize: "0.9rem" }}
                  type="button"
                  className="btn btn-block mt-0 btn-danger"
                  data-toggle="button"
                  aria-pressed="false"
                >
                  Eliminar
                </button>
                <hr />
              </div>
            </div>
          );
        })}
        <div className="row mt-5">
          <div className="col-12">
            <ProductDetailStore
              categories
              idProduct={activeProduct.idProduct}
              activeProduct={activeProduct.state}
              handleClose={toggleProductStore}
            />
          </div>
        </div>
      </div>
    </>
  );
};
export default ProductsByStore;
