<?php

namespace Models;
use Config\Database;
use PDO;

class Producto {

    public $table_name = "productos";

    private $cod_producto;
    private $stock;
    private $poster;
    private $descripcion;
    private $nombre_producto;
    private $precio;
    private $cod_categoria;

    

    public function categoryExists($nombre_cat){
        $database = new Database();
        $db = $database->getConnection();

        $query = "SELECT *
        FROM categorias_productos
        WHERE nombre_cat = ?
        LIMIT 1";

        $stmt = $db->prepare($query);
        $stmt->bindParam(1, $nombre_cat , PDO::PARAM_STR);
        
        $stmt->execute();

        $num = $stmt->rowCount();

        if($num > 0){
            
            return true;
        }
    
        return false;
    }


    public function addCategory($nombre_cat){
        $database = new Database();
        $db = $database->getConnection();

        $query = "INSERT INTO categorias_productos(nombre_cat) VALUES(?)";

        $stmt = $db->prepare($query);
        $stmt->bindParam(1, $nombre_cat , PDO::PARAM_STR);
        
        $stmt->execute();
    }




    /**
     * Get the value of cod_producto
     */ 
    public function getCod_producto()
    {
        return $this->cod_producto;
    }

    /**
     * Set the value of cod_producto
     *
     * @return  self
     */ 
    public function setCod_producto($cod_producto)
    {
        $this->cod_producto = $cod_producto;

        return $this;
    }

    /**
     * Get the value of stock
     */ 
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set the value of stock
     *
     * @return  self
     */ 
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get the value of poster
     */ 
    public function getPoster()
    {
        return $this->poster;
    }

    /**
     * Set the value of poster
     *
     * @return  self
     */ 
    public function setPoster($poster)
    {
        $this->poster = $poster;

        return $this;
    }

    /**
     * Get the value of descripcion
     */ 
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set the value of descripcion
     *
     * @return  self
     */ 
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get the value of nombre_producto
     */ 
    public function getNombre_producto()
    {
        return $this->nombre_producto;
    }

    /**
     * Set the value of nombre_producto
     *
     * @return  self
     */ 
    public function setNombre_producto($nombre_producto)
    {
        $this->nombre_producto = $nombre_producto;

        return $this;
    }

    /**
     * Get the value of precio
     */ 
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set the value of precio
     *
     * @return  self
     */ 
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get the value of cod_categoria
     */ 
    public function getCod_categoria()
    {
        return $this->cod_categoria;
    }

    /**
     * Set the value of cod_categoria
     *
     * @return  self
     */ 
    public function setCod_categoria($cod_categoria)
    {
        $this->cod_categoria = $cod_categoria;

        return $this;
    }
}


?>