import React from "react";
import InformationUser from "../components/InformationUser";
import ListGroupUser from "../components/ListGroupUser";
import Orders from "../components/Orders";
import { connect } from "react-redux";

const UserPanel = function UserPanel({ location, user }) {
  const action = location.search.split("=")[1];

  return (
    <div className="container-fluid mt-4">
      <div className="row">
        <div className="col-12">
          <h2 className="mb-3">Panel de Usuario</h2>
          <hr />
        </div>
      </div>
      <div className="row mt-5 mb-5">
        <div className="col-4">
          <ListGroupUser location={action} />
        </div>

        <div className="col-8 position">
          <div className="tab-content" id="nav-tabContent col-12">
            <div
              className="tab-pane fade show active"
              id="list-home"
              role="tabpanel"
              aria-labelledby="list-home-list"
            ></div>
            {action == "mis-pedidos" ? <Orders user="user" /> : ""}
            {action == "historial-de-pedidos" ? <Orders user="user" /> : ""}
            {action == "informacion-usuario" ? <InformationUser /> : ""}
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};

export default connect(mapStateToProps, null)(UserPanel);
