import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

const CategoryStore = function CategoryStore({ match }) {
  const [store, setStore] = useState([]);

  useEffect(() => {
    async function getStoreByCategory() {
      const storesByCategory = await axios(
        "https://hn.algolia.com/api/v1/search?query=redux"
      );
      setStore(storesByCategory.data.hits);
    }

    getStoreByCategory();
  }, []);

  return (
    <div className="container-fluid">
      <div className="row mt-4">
        <div className="col-12">
          <h3 className="mb-3">Tiendas Recomendadas</h3>
        </div>
      </div>
      <div className="row mb-5">
        <div className="col-12">
          <hr />
        </div>
        <div className="col-12">
          <h5>
            Filtrado por :{" "}
            <span style={{ color: "white" }} className=" badge badge-secondary">
              {match.params.category}
            </span>
          </h5>
        </div>
      </div>
      <div
        className="row m-auto justify-content-center "
        style={{ width: "80%" }}
      >
        {store.map((value, index) => {
          return (
            <div className="col-3" key={index}>
              <div className="card border-0 rounded">
                <Link to="/tiendas/metro">
                  <img
                    src="https://images.rappi.pe/store_type/market-1598710455.png?d=200x200&e=webp"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body">
                    <h5 className="card-title text-center text-dark">
                      Metro Barranco
                    </h5>
                    <h6 className="card-title text-center text-muted">
                      Supermercados
                    </h6>
                  </div>
                </Link>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default CategoryStore;
