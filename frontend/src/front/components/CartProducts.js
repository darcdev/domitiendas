import React from "react";
import { connect } from "react-redux";
import { incrementProduct, decrementProduct } from "../actions/index";
import "../assets/styles/components/CartProducts.css";

const CartProducts = function CartProducts({
  products,
  incrementProduct,
  decrementProduct,
}) {
  const handleIncrement = (id) => {
    incrementProduct(id);
  };
  const handleDecrement = (id) => {
    decrementProduct(id);
  };
  return (
    <div>
      {products.map((value, index) => {
        return (
          <div key={index}>
            <div className="media mb-3">
              <img
                style={{ width: "65px" }}
                src={value.poster}
                className="align-self-center mr-3"
                alt="..."
              />
              <div className="media-body" style={{ position: "relative" }}>
                <div className="cont-info">
                  <button
                    onClick={() => handleDecrement(value.id)}
                    className="mr-4"
                  >
                    {value.cant === 1 ? (
                      <img
                        style={{ width: "15px" }}
                        src="https://img.icons8.com/android/24/000000/trash.png"
                        alt=""
                      />
                    ) : (
                      "-"
                    )}{" "}
                  </button>
                  {value.cant}{" "}
                  <button
                    onClick={() => handleIncrement(value.id)}
                    className="ml-3"
                  >
                    {" "}
                    {value.cant < value.stock ? "+" : ""}
                  </button>
                </div>
                <h6 className="mt-0 mb-1 mt-2">{value.name} </h6>
                <p style={{ width: "280px" }} className="text-small">
                  ${value.total}
                </p>
                <p style={{ width: "280px" }} className="text-small ">
                  {value.description}
                </p>
              </div>
            </div>
            <hr />
          </div>
        );
      })}
    </div>
  );
};
const mapStateToProps = (state) => {
  return {
    products: state.cart.products,
  };
};

const mapDispatchToProps = {
  incrementProduct,
  decrementProduct,
};
export default connect(mapStateToProps, mapDispatchToProps)(CartProducts);
